import telebot
from save_data import SaveData
from database.text import Text
import time

token = '104348320:AAF3PQeAM7SJWJzpcXbHPZz1pIH2gJtcCoY'  # pyRezaBot
bot = telebot.AsyncTeleBot(token)
password = 'tms'


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    try:
        print('chat id: {}  name: {}'.format(message.chat.id, message.text))
        bot.reply_to(message, Text.welcome)
        save_data(message)
    except Exception as e:
        print('error in send_welcome fun: {}'.format(e))
        bot.reply_to(message, Text.error)


@bot.message_handler(commands=['config'])
def set_config(message):
    try:
        bot.reply_to(message, Text.enter_pass)
        bot.register_next_step_handler(message, check_password)
        save_data(message)
    except Exception as e:
        print('error in set_config fun: {}'.format(e))
        bot.reply_to(message, Text.error)


def check_password(message):
    text = message.text
    if text == password:
        bot.reply_to(message, Text.plz_set)
        bot.register_next_step_handler(message, get_config_message)

    elif text == 'cancel':
        bot.reply_to(message, Text.cancel)

    else:
        bot.reply_to(message, Text.pass_is_invalid)
        bot.register_next_step_handler(message, check_password)
    save_data(message)


def get_config_message(message):
    try:
        bot.reply_to(message, Text.good_luck)
        save = SaveData('database/database')  # initialize
        save.set_next_class(message.text, message.chat.id)
    except Exception as e:
        print('error in get_config fun: {}'.format(e))
        bot.reply_to(message, Text.error)


@bot.message_handler(commands=['next_class'])
def send_next_class(message):
    try:
        save = SaveData('database/database')  # initialize
        bot.reply_to(message, save.next_class())
        save.save_message(chat_id=message.chat.id, message=message.text)
    except Exception as e:
        print('error in send_next_class fun: {}'.format(e))
        bot.reply_to(message, Text.error)


@bot.message_handler(func=lambda message: True)
def listener(message):
    try:
        # if message.text == 'به عمو ها سلام کن':
        #     bot.send_message(message.chat.id, 'سلام')
        # elif message.text == 'آفرین':
        #     bot.send_message(message.chat.id, 'چاکریم')
        save_data(message)
    except Exception as e:
        print('error in listener fun: {}'.format(e))
        bot.reply_to(message, Text.error)


def save_data(message):
    try:
        save = SaveData('database/database')  # initialize
        save.save_message(chat_id=message.chat.id, message=message.text)
    except Exception as e:
        print('error in save_data fun: {}'.format(e))
        bot.reply_to(message, Text.error)


def main():
    try:
        bot.polling()
    except Exception as e:
        print('error in main fun: {}'.format(e))

    # bot.polling(none_stop=True)
    while True:  # Don't let the main Thread end.
        time.sleep(1)

if __name__ == '__main__':
    main()
