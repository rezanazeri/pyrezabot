import sqlite3


class SaveData:
    def __init__(self, database_name='database/database'):
        try:
            self._db = sqlite3.connect(database=database_name)
            print('database connected')
        except Exception as e:
            print('Error in open database: ' + str(e))

    def save_message(self, chat_id, message):
        try:
            self._db.execute(
                'INSERT OR ignore INTO chat_id (id) VALUES ({})'.format(chat_id))  # save chat id in database
            self._db.execute(
                r'INSERT INTO message (chat_id, message) VALUES ({}, "{}")'.format(chat_id, message))
            self._db.commit()
            print('id and message saved successfully')
        except Exception as e:
            print('Error in saving data: ' + str(e))

    def next_class(self):
        try:
            cursor = self._db.execute(
                'SELECT * FROM time WHERE ID = (SELECT MAX(ID) FROM time)')
            return cursor.fetchone()[1]
        except Exception as e:
            print('Error in saving data: ' + str(e))
            return ''

    def set_next_class(self, text, chat_id):
        try:
            self._db.execute(
                r'INSERT INTO time (message, chat_id) VALUES ("{}", {})'.format(text, chat_id))
            self._db.commit()
            print('class time saved successfully')
        except Exception as e:
            print('Error in saving data: ' + str(e))

    def __del__(self):
        self._db.close()
        print('database closed\n')
